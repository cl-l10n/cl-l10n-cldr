;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem cl-l10n-cldr
  :name "CL-L10N-CLDR"
  :version "1.8.0"
  :description "The necessary CLDR files for cl-l10n packaged in a QuickLisp friendly way.")
